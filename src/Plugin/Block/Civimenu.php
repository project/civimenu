<?php /**
 * @file
 * Contains \Drupal\civimenu\Plugin\Block\Civimenu.
 */

namespace Drupal\civimenu\Plugin\Block;

use Drupal\civicrm\Civicrm;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Civimenu block.
 *
 * @Block(
 *   id = "civimenu_civimenu",
 *   admin_label = @Translation("CiviCRM Menu")
 * )
 */
class Civimenu extends BlockBase  implements ContainerFactoryPluginInterface {

  /**
   * Class constructor.
   *
   * @param \Drupal\civicrm\Civicrm $civicrm
   *   The CiviCRM service.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(Civicrm $civicrm, array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $civicrm->initialize();
    \Civi::resources()->addCoreResources();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('civicrm'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => Markup::create('<div id="crm-container"></div>'),
      '#cache' => ['max-age' => 0],
    ];
  }


}
